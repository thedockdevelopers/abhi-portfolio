import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
    private userDataPostSubject = new BehaviorSubject<any>(null);
    userDataPostObservable: Observable<any> = this.userDataPostSubject.asObservable();

  constructor(private angularFirestore: AngularFirestore) {}

  insertUserDetailsIntoDatabase(userDetails: any) {
    this.angularFirestore.collection('contacts').doc(userDetails.email).set(userDetails).then(()=>{
      this.userDataPostSubject.next('stored');
    },(error)=>{
      this.userDataPostSubject.next('notstored');
    });
  }

}