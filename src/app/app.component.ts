import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { timer } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Personal-Portfolio';
  private subscription: Subscription | undefined;
  private timer: Observable<any> | undefined;
  showLoader = true;
  prevScrollpos: any;
  constructor(public router: Router) { }

  ngOnInit(): void {
    this.setTimer();
    this.prevScrollpos = window.pageYOffset;
  }

  ngOnDestroy() {
    if (this.subscription && this.subscription instanceof Subscription) {
      this.subscription.unsubscribe();
    }
  }

  public setTimer() {
    this.showLoader = true;
    this.timer = timer(1300);
    this.subscription = this.timer.subscribe(() => {
      this.showLoader = false;
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.showLoader = false;
    }, 1300)
  }


  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event: Event) {
    var currentScrollPos = window.pageYOffset;
    if (this.prevScrollpos > currentScrollPos) {
      document.getElementById("navbar").style.background = "0% 0% / contain #1b1b1b";
      document.getElementById("navbar").style.top = "0";
      if(window.scrollY == 0) {
      document.getElementById("navbar").style.background = "none";
      }
    } else {
      document.getElementById("navbar").style.top = "-80px";
    }
    this.prevScrollpos = currentScrollPos;
  }
}