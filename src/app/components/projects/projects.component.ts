import { ContentfulService } from './../../shared/services/contentful.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import "core-js/stable";
import "regenerator-runtime/runtime"; 
import { Observable } from 'rxjs';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],

})
export class ProjectsComponent implements OnInit {

  projects$: Observable<any>;
  projects: any;

  constructor(private contentfulApiService: ContentfulService) { }

  ngOnInit() { 
    this.projects$ = this.contentfulApiService.getDetails('projects');
    this.projects = [
      {
        liveUrl: 'http://dockdevelopers.com/',
        gitUrl: 'https://gitlab.com/thedockdevelopers/dock-developers',
        title: 'Dock Developers Dashboard',
        desc: 'A react website which showcase the apps & websites provided by Dock Developers',
        tech: ['React', 'SCSS', 'NodeJS', 'Firebase']
      },
      {
        liveUrl: 'https://covitrail.dockdevelopers.com/',
        gitUrl: 'https://gitlab.com/thedockdevelopers/covitrail',
        title: 'Website to track Covid-19 outbreak',
        desc: 'Angular website build on top of Ionic Framework which is used to track Covid-19 outbreak and to get news, videos and stats live feed.',
        tech: ['Angular', 'SCSS', 'NodeJS', 'Ionic', 'Firebase']
      },
      {
        liveUrl: 'https://dshort.in/',
        gitUrl: 'https://gitlab.com/thedockdevelopers/dshort',
        title: 'Website to short the URLs',
        desc: 'A react website which short the URL, provide short links and QR Code to open the website using any QR Code scanner',
        tech: ['React', 'SCSS', 'NodeJS', 'Firebase']
      }
    ];
  }

}
