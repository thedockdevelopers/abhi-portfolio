import { ContentfulService } from './../../shared/services/contentful.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import "core-js/stable";
import "regenerator-runtime/runtime"; 
import { Observable } from 'rxjs';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ExperienceComponent implements OnInit {

  jobs$: Observable<any>;
  jobs: any;

  constructor(private contentfulApiService: ContentfulService) { }

  ngOnInit() {
    this.jobs$ = this.contentfulApiService.getDetails('jobs');
    this.jobs = [
      {
        status: 'IVY Software',
        title: 'Software Engineer',
        company: 'IVY Software',
        description: [
          "Working on the dashboards of an online marketplace, building new features, writing resuable and maintainable code",
          "Working with modern web frameworks &amp; tools as a part of Frontend team",
          "Learning best practices and approaches to write clean code"
        ]
      },
      {
        status: 'Training',
        title: 'Trainee Software engineer',
        company: 'Infosys',
        description: [
          "Working on the dashboards of an online marketplace, building new features, writing resuable and maintainable code",
          "Working with modern web frameworks &amp; tools as a part of Frontend team",
          "Learning best practices and approaches to write clean code"
        ]
      },
      {
        status: 'Internship',
        title: 'Intern Software Engineer',
        company: 'Editmeet',
        description: [
          "Working on the dashboards of an online marketplace, building new features, writing resuable and maintainable code",
          "Working with modern web frameworks &amp; tools as a part of Frontend team",
          "Learning best practices and approaches to write clean code"
        ]
      }
    ];
  }

}
