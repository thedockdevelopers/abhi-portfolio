import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { NguCarousel, NguCarouselConfig } from '@ngu/carousel';

@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.scss'],
})

export class TestimonialComponent implements OnInit, AfterViewInit {
  testimonial: any;
  carouselConfig: NguCarouselConfig;
  @ViewChild('myCarousel') myCarousel: NguCarousel<any>;
  constructor(
  ) {
   }

  ngOnInit() {
    this.carouselConfig = {
      grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
      load: 3,
      interval: {timing: 9000, initialDelay: 1000},
      loop: true,
      touch: true,
      //velocity: 0.1,
      speed: 800
    }
    this.testimonial = [
      {
        name: 'Suresh Babu Veluri',
        image: '../../../assets/images/suresh.png',
        designation: 'Engineering Manager',
        company: 'IVY Software',
        desc: '“Thanks for stepping up and taking responsibilities in the team to fill senior resources place. All the best, appreciated..!”'
      },
      {
        name: 'Gomathy Sankara Narayanan',
        image: '../../../assets/images/gomathy.png',
        designation: 'Chief Delivery Officer',
        company: 'GVC',
        desc: '“Being a newbie, Abhishek has been adding great value to the team\'s growth, a very fast learner and his positive attitude towards work is appreciated. Thank you and keep it up.”'
      },
      {
        name: 'Amit Jugran',
        image: '../../../assets/images/amit.png',
        designation: 'Founding Partner',
        company: 'EditMeet',
        desc: '“We thank Abhishek for being a part of EditMeet in its starting-up journey. We appreciate the fantastic job, he has done in last 1.5 months. His hardwork and dedication has really helped us to achieve set objectives well in time. We particular, want to thank him for his ideas to better the existing workflow processes and willingness to take the challenging tasks.”'
      }
    ];
  }

  ngAfterViewInit() {
    this.myCarousel.moveTo(0);
  }

  moveTo(slide) {
    this.myCarousel.moveTo(slide);
  }
}
