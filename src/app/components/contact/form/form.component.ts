import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from '@angular/material/dialog';
import { Subscriber, Subscription } from "rxjs";
import { FirebaseService } from '../../../shared/services/firebase.service';

@Component({
    selector: 'app-contact-form',
    templateUrl: 'form.component.html',
    styleUrls: ['form.component.scss']
})

export class Form implements OnInit, OnDestroy {
    @ViewChild('form') form: ElementRef;
    submitted: boolean;
    error: boolean;
    contactSubs: Subscription;
    contactForm = new FormGroup({
        name: new FormControl('', Validators.required),
        email: new FormControl('', Validators.compose([Validators.required, 
            Validators.pattern(RegularExpressionConstant.EMAIL)])),
        message: new FormControl('')

    });
    constructor(public dialogRef: MatDialogRef<Form>,
        private firebaseService: FirebaseService) {
    }

    ngOnInit() {
        this.contactSubs = this.firebaseService.userDataPostObservable.subscribe((response: any) => {
            if(response == 'stored') {
                this.submitted = true;
                this.resetForm(false);
                this.error = false;
            }
            else {
                this.submitted = false;
                this.error = false;
            }
        });
    }

    resetForm(clearAll: boolean) {
        if(clearAll) {
            this.submitted = false; 
        }
        this.error = false;
        if(this.contactForm) {
           this.contactForm.reset();
        }
    }

    close() {
        this.submitted = false;
        this.error = false;
        this.dialogRef.close();
    }

    submit(contactFormValue: any) {
        this.submitted = false;
        this.error = false;
        if(this.contactForm.invalid) {
            this.error = true;
        }
        else {
            this.firebaseService.insertUserDetailsIntoDatabase(contactFormValue);
        }
    }

    ngOnDestroy() {
        if(this.contactSubs) {
            this.contactSubs.unsubscribe();
        }
    }
}

export class RegularExpressionConstant { 
    static EMAIL: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
}