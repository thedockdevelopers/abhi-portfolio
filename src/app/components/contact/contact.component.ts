import { Component, OnInit } from '@angular/core';
import { Form } from './form/form.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})

export class ContactComponent implements OnInit {

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  openFormDialog() {
    this.dialog.open(Form, {
      height: '100vh',
      width: '100vw',
      panelClass: 'contact-form-dialog',
      direction: 'ltr'
    });
  }

}
