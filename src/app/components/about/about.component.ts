import { ContentfulService } from './../../shared/services/contentful.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import "core-js/stable";
import "regenerator-runtime/runtime"; 
import { Observable } from 'rxjs';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AboutComponent implements OnInit {

  about$: Observable<any>;
  about: any;
  skills: any;
  
  constructor(private contentfulApiService: ContentfulService) { }

  ngOnInit(): void {
    this.about$ = this.contentfulApiService.getDetails('about');
    this.about = [
      {desc:"Hello! I'm Abhishek Maheshwari, a Full-Stack developer based in India."},
      {desc:"I am passionate about almost everything related to technology and the internet. I enjoy learning new technology stacks. My interests range from learning Web/Mobile/Desktop app development to almost everything."},
      {desc:"While studying at university, I started freelancing alongside and worked with a friend for a while. After getting some work experience I joined Seerbytes where I worked on several intersting projects and had a lot of learning. I completed my graduatioin in March 2021, and I am open to work opportunities."}
    ];
    this.skills = [
      {skillList: ["Angular", "RxJs", "HTML & (S)CSS", "TypeScript", "Javascript (ES6)", "NodeJS"]}
    ];
  }


}

